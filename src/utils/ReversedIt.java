package utils;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Jean Marcos da Rosa - jeanmarcosdarosa@gmail.com
 */
public class ReversedIt<T> implements Iterable<T> {
    private final List<T> origin;

    public ReversedIt(List<T> origin) {
        this.origin = origin;
    }

    @Override
    public Iterator<T> iterator() {
        final ListIterator<T> i = origin.listIterator(origin.size());

        return new Iterator<T>() {
            @Override
            public boolean hasNext() { return i.hasPrevious(); }
            @Override
            public T next() { return i.previous(); }
            @Override
            public void remove() { i.remove(); }
        };
    }

    public static <T> ReversedIt<T> reversed(List<T> original) {
        return new ReversedIt<T>(original);
    }
}
