package normalizer;

import beans.Line;
import utils.ReversedIt;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 * Copyright (c) 2015 Jean Marcos da Rosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author Jean Marcos da Rosa - jeanmarcosdarosa@gmail.com
 * @see https://JeanMarcosDaRosa@bitbucket.org/JeanMarcosDaRosa/pynorm
 */
public class Normalizer {

    static String _version_ = "0.0.2";
    static String _url_ = "https://JeanMarcosDaRosa@bitbucket.org/JeanMarcosDaRosa/pynorm";
    static boolean debug = false, allFiles = false, clearBS = false, signiture = true;
    static String filenameSrc = null, filenameDst = null, folderSrc = null;
    static Integer cIdent = 2;
    static String charset = null;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        readParams(args);
        List<File> files = new ArrayList<>();
        if (folderSrc != null) {
            File fSrc = new File(folderSrc);
            if (fSrc.exists() && fSrc.isDirectory()) {
                for (File f : fSrc.listFiles()) {
                    if (f.getName().endsWith(".py")) {
                        files.add(f);
                    }
                }
            } else {
                System.out.println("Folder [" + folderSrc + "] not found!");
                System.exit(1);
            }
        } else {
            files.add(new File(filenameSrc));
        }
        for (File sourceFile : files) {
            Integer beforeIdent = 0, level = 0, lineId = 0;
            if (sourceFile.exists() && !sourceFile.isDirectory()) {
                List<String> lines = null;
                if (charset != null) {
                    lines = FileUtils.readLines(sourceFile, charset);
                } else {
                    lines = FileUtils.readLines(sourceFile);
                }
                List<Line> positionLines = new ArrayList<>();
                Line lineToClear = null;
                for (String content : lines) {
                    int ident = spaceBeginContent(content);
                    if (ident != beforeIdent && !content.trim().equals("")) {
                        if (ident > beforeIdent) {
                            level++;
                        } else {
                            for (Line l : ReversedIt.reversed(positionLines)) {
                                if (!l.getBlank() && ident == l.getStart()) {
                                    level = l.getLevel();
                                    break;
                                }
                            }
                        }
                        beforeIdent = ident;
                    }
                    Line newLine = new Line(++lineId, level, ident, content);
                    positionLines.add(newLine);
                    if (!newLine.getBlank()) {
                        testInsertSpace(newLine, lineToClear);
                        lineToClear = newLine;
                    }
                }
                if (debug) {
                    dumpLines(positionLines);
                }
                normalizeFile(positionLines, cIdent, (folderSrc != null) ? sourceFile : ((filenameDst != null) ? (new File(filenameDst)) : null));
            } else {
                System.out.println(String.format("Source File: %s not found.", filenameSrc));
            }
        }
    }

    private static void readParams(String[] args) {
        boolean ok = false, levelFlag = false, charsetFlag = false, oFlag = false, dFlag = false;
        try {
            for (String arg : args) {
                if (levelFlag) {
                    cIdent = Integer.parseInt(arg);
                    levelFlag = false;
                } else if (charsetFlag) {
                    charset = arg;
                    charsetFlag = false;
                } else if (oFlag) {
                    filenameSrc = arg;
                    oFlag = false;
                    ok = true;
                } else if (dFlag) {
                    filenameDst = arg;
                    dFlag = false;
                } else if (allFiles && folderSrc == null) {
                    folderSrc = arg;
                    dFlag = false;
                } else {
                    if (arg.equals("--debug") || arg.equals("-d")) {
                        debug = true;
                    } else if (arg.equals("--level") || arg.equals("-l")) {
                        levelFlag = true;
                    } else if (arg.equals("--charset") || arg.equals("-c")) {
                        charsetFlag = true;
                    } else if (arg.equals("--clear-spaces")) {
                        clearBS = true;
                    } else if (arg.equals("--no-signiture")) {
                        signiture = false;
                    } else if (arg.equals("-o")) {
                        oFlag = true;
                    } else if (arg.equals("-d")) {
                        dFlag = true;
                    } else if (arg.equals("--all")) {
                        allFiles = true;
                        ok = true;
                    }
                }
            }

        } catch (NumberFormatException e) {
            ok = false;
        }
        if (!ok || levelFlag || charsetFlag || oFlag || dFlag || ((filenameSrc != null || filenameDst != null) && allFiles)) {
            System.out.println("Use: java -jar PyNorm.jar [options]");
            System.out.println("\t--charset / -c \"utf-8\"");
            System.out.println("\t--debug / -d");
            System.out.println("\t--clear-spaces remove desnecessary blank lines");
            System.out.println("\t--level / -l [1,2,3,4,5 ... identation]");
            System.out.println("\t-o / -d origin file, destination file");
            System.out.println("\t--all [folder] all files \".py\" on folder ** BACKUP IS RECOMENDED BEFORE");
            System.out.println("\t--no-signiture not insert PyNorm signiture to end file");
            System.exit(1);
        }
    }

    private static void dumpLines(List<Line> positionLines) {
        System.out.println("\n----------- Dump schema ------------\n");
        for (Line l : positionLines) {
            System.out.println(l);
        }
        System.out.println("\n----------------------------------\n");
    }

    private static void normalizeFile(List<Line> positionLines, Integer cIdent, File wDst) {
        boolean inc = true;
        String out = "";
        for (Line line : positionLines) {
            String outLine = line.getContent();
            if (line.getLevel() > 0) {
                if (!line.getBlank()) {
                    outLine = normalizeLine(line.getContent(), line.getLevel(), cIdent);
                }
            }
            if (clearBS) {
                if (!line.getBlank()) {
                    if (line.getInsertSpace()) {
                        out += outLine + "\n\n";
                    } else {
                        out += outLine + "\n";
                    }
                }
            } else {
                out += outLine + "\n";
            }
            if (outLine.toLowerCase().contains("normalized by pynorm")) {
                inc = false;
            }
        }
        if (inc && signiture) {
            out += "\n\n\n# Normalized by PyNorm " + _version_ + " - " + _url_;
            out += "\n# Date: " + (new Date()) + "\n";
        }
        if (wDst != null && !debug) {
            try {
                if (charset != null) {
                    FileUtils.writeStringToFile(wDst, out, charset);
                } else {
                    FileUtils.writeStringToFile(wDst, out);
                }
            } catch (IOException ex) {
                Logger.getLogger(Normalizer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println(out);
        }
    }

    private static String normalizeLine(String str, Integer breaks, Integer cIdent) {
        if (str != null) {
            str = str.trim();
            for (int i = 0; i < (cIdent * breaks); i++) {
                str = " " + str;
            }
        }
        return str;
    }

    private static int spaceBeginContent(String content) {
        int ident = 0;
        for (Character c : content.toCharArray()) {
            if (c.equals(' ') || c.equals('\t')) {
                ident++;
            } else {
                break;
            }
        }
        return ident;
    }

    private static void testInsertSpace(Line newLine, Line lineToClear) {
        if (clearBS && newLine != null && lineToClear != null) {
            String aux = newLine.getContent().trim();
            if ((aux.startsWith("def") || aux.startsWith("class")) || (!aux.startsWith("import") && lineToClear.getContent().trim().startsWith("import"))) {
                lineToClear.setInsertSpace(true);
            }
        }
    }

}
