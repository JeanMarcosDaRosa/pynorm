package beans;

/**
 *
 * @author Jean Marcos da Rosa - jeanmarcosdarosa@gmail.com
 */
public class Line {
    private Integer id;
    private Integer level;
    private Integer start;
    private Boolean blank;
    private Boolean insertSpace;
    private String content;

    public Line(Integer id, Integer level, Integer start, String content) {
        this.id = id;
        this.level = level;
        this.start = start;
        this.content = content;
        this.insertSpace = false;
        this.blank = ((content==null)||(content.trim().equals("")));
    }

    @Override
    public String toString() {
        return "Line{ " + id + ", level=" + level + ", start=" + start +", blank=" + blank + ", insertSpace=" + insertSpace + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Line other = (Line) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return the start
     */
    public Integer getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Integer start) {
        this.start = start;
    }

    /**
     * @return the blank
     */
    public Boolean getBlank() {
        return blank;
    }

    /**
     * @param blank the blank to set
     */
    public void setBlank(Boolean blank) {
        this.blank = blank;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the insertSpace
     */
    public Boolean getInsertSpace() {
        return insertSpace;
    }

    /**
     * @param insertSpace the insertSpace to set
     */
    public void setInsertSpace(Boolean insertSpace) {
        this.insertSpace = insertSpace;
    }
}
